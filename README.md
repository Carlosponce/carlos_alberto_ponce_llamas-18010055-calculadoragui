using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculadoraGrafica
{
    public partial class Nmbre : Form
    {
        double primero;
        double segundo;
        double resultado;
        string operador;
        public Nmbre()
        {
            InitializeComponent();
        }

        private void button43_Click(object sender, EventArgs e)
        {
            TextBox.Text = TextBox.Text += "0";
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            TextBox.Text = TextBox.Text += "1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            TextBox.Text = TextBox.Text += "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            TextBox.Text = TextBox.Text += "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            TextBox.Text = TextBox.Text += "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            TextBox.Text = TextBox.Text += "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            TextBox.Text = TextBox.Text += "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            TextBox.Text = TextBox.Text += "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            TextBox.Text = TextBox.Text += "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            TextBox.Text = TextBox.Text += "9";
        }

        private void btnpunto_Click(object sender, EventArgs e)
        {
            TextBox.Text = TextBox.Text += ".";
        }

        private void btnsuma_Click(object sender, EventArgs e)
        {
            operador = "+";
            primero = double.Parse(TextBox.Text);
            TextBox.Clear();
                }

        private void btnresta_Click(object sender, EventArgs e)
        {
            operador = "-";
            primero = double.Parse(TextBox.Text);
            TextBox.Clear();
        }

        private void btnmulti_Click(object sender, EventArgs e)
        {
            operador = "*";
            primero = double.Parse(TextBox.Text);
            TextBox.Clear();
        }

        private void btndivi_Click(object sender, EventArgs e)
        {
            operador = "/";
            primero = double.Parse(TextBox.Text);
            TextBox.Clear();
        }

        private void btnigual_Click(object sender, EventArgs e)
        {
            segundo = double.Parse(TextBox.Text);

            switch (operador) {
                case "+":
                    resultado = primero + segundo;
                    TextBox.Text = resultado.ToString();
                    break;
                case "-":
                    resultado = primero - segundo;
                    TextBox.Text = resultado.ToString();
                    break;
                case "*":
                    resultado = primero * segundo;
                    TextBox.Text = resultado.ToString();
                    break;
                case "/":
                    resultado = primero / segundo;
                    TextBox.Text = resultado.ToString();
                    break;
            }

        }

        private void btnelimina_Click(object sender, EventArgs e)
        {
            TextBox.Clear();

        }

        private void btnborra_Click(object sender, EventArgs e)
        {
            if(TextBox.Text.Length == 1)
                TextBox.Text = "";
            else
                TextBox.Text = TextBox.Text.Substring(0, TextBox.Text.Length - 1);
        }
    }
}
